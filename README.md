IP Country Logger Drupal Module

Introduction
------------

 This module assists in determining the geographic location associated with an 
 IP address, providing insights into the user's country. It stores details of 
 the country trace linked to the IP address of the currently logged-in user 
 in a logger.

Features
------------
* Geolocation Identification: Accurately determines the country associated 
  with an IP address.
* Logging Functionality: Logs details of the country trace linked to the IP 
  address of the current logged-in user.
* User-Friendly Interface: Intuitive interface for easy navigation and usage.
* Configurable Options: Allows users to configure settings to tailor the module 
  according to specific requirements.

Usage
------------
* Installation: Install the IP Country Logger Module following the instructions 
  outlined in the Installation section below.
* Configuration: From the administration page, users can configure additional 
  settings, view logs, and perform other module-related tasks as needed.
* User Geolocation Data: Access detailed geolocation data for each logged-in 
  user,including the country, region, and city associated with their IP address.
  This data can be useful for personalizing content or enhancing user experience
* Viewing Logs: To view logged country trace details, navigate to the module's 
  administration page (admin/config/countrytrace).Here,you can see comprehensive
  list of logged entries with associated IP addresses and their respective 
  countries.

Installation Process of Module
------------
  * Navigate to /admin/modules and install the IP Country Logger module.
  * After enabling the module, each time a user logs in, the module will 
    trace the country using the IP address of the currently logged-in user and 
    save this information in the Drupal Logger with the type ip_country_logger.

Install as you would normally install a contributed Drupal module.
------------
 * See: https://www.drupal.org/docs/extending-drupal/installing-modules
   for further information.


MAINTAINERS
-----------
This module is actively maintained by its primary developer. If you have any 
questions, concerns, or issues, feel free to reach out.

 * Harwinder Singh
    * Drupal.org Profile: https://www.drupal.org/u/Harwinder
 * Gurinderpal Singh
    * Drupal.org Profile: https://www.drupal.org/u/gurinderpal-lnwebworks
