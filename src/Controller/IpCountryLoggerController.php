<?php

namespace Drupal\ip_country_logger\Controller;

use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for fetching user's country information based on IP address.
 */
class IpCountryLoggerController extends ControllerBase {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructs a IpCountryLoggerController object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request stack.
   */
  public function __construct(ClientInterface $http_client, RequestStack $request_stack) {
    $this->httpClient = $http_client;
    $this->currentRequest = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('request_stack')
    );
  }

  /**
   * Fetch user's country information based on IP address and return as JSON.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response containing country name.
   */
  public function getCountryName() {
    $public_ip = file_get_contents('https://api.ipify.org');
    $url = 'http://ip-api.com/json/' . $public_ip;

    try {
      $response = $this->httpClient->request('GET', $url);
      $data = json_decode($response->getBody()->getContents(), TRUE);

      if (isset($data['country'])) {
        return new JsonResponse([
          'country' => $data['country'],
          'IP' => $data['query'],
        ]);
      }
      else {
        return new JsonResponse(['error' => 'Unable to fetch country information.'], 500);
      }
    }
    catch (\Exception $e) {
      return new JsonResponse(['error' => 'Unable to fetch IP information.'], 500);
    }
  }

}
